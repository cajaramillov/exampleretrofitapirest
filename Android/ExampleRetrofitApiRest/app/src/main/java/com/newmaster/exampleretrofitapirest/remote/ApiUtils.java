package com.newmaster.exampleretrofitapirest.remote;

public class ApiUtils
{
    private ApiUtils(){}

    public static final String API_URL = "https://jsonplaceholder.typicode.com/";

    public static PostService getPostService()
    {
        return RetrofitClient.getClient(API_URL).create(PostService.class);
    }
}
