package com.newmaster.loginactivity.shared_preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class SharedPreferencesManager
{
    private SharedPreferences sharedPreferences;

    public SharedPreferencesManager(Context context)
    {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public String getTokenApi()
    {
        return sharedPreferences.getString(Util.TOKEN_API, null);
    }

    public void setTokenApi(String token)
    {
        sharedPreferences.edit().putString(Util.TOKEN_API, token).apply();
    }

    public String getIdUser()
    {
        return sharedPreferences.getString(Util.ID_USER, null);
    }

    public void setIdUser(String idUser)
    {
        sharedPreferences.edit().putString(Util.ID_USER, idUser).apply();
    }
}
