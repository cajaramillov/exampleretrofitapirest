package com.newmaster.loginactivity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.newmaster.loginactivity.model.Token;
import com.newmaster.loginactivity.model.User;
import com.newmaster.loginactivity.model.UserAuth;
import com.newmaster.loginactivity.remote.ApiUtils;
import com.newmaster.loginactivity.remote.UserAuthService;
import com.newmaster.loginactivity.remote.UserService;
import com.newmaster.loginactivity.shared_preferences.SharedPreferencesManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
{
    private EditText editUsername;
    private EditText editPassword;
    private UserService userService;
    private UserAuthService userAuthService;
    private Token userToken;
    private UserAuth userAuth;
    private SharedPreferencesManager sharedPreferencesManager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Login");
        sharedPreferencesManager = new SharedPreferencesManager(getApplicationContext());
        editUsername = (EditText) findViewById(R.id.editUsername);
        editPassword = (EditText) findViewById(R.id.editPassword);
        userService = ApiUtils.getUserService();
        userAuthService = ApiUtils.getUserAuthService();
    }

    public void loginWebService(View view)
    {
        boolean cancel = false;
        View focusView = null;

        // Reset errors.
        editUsername.setError(null);
        editPassword.setError(null);

        // Store values at the time of the login attempt.
        String username = editUsername.getText().toString();
        String password = editPassword.getText().toString();


        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password))
        {
            editPassword.setError(getString(R.string.error_field_required));
            focusView = editPassword;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(username))
        {
            editUsername.setError(getString(R.string.error_field_required));
            focusView = editUsername;
            cancel = true;
        }


        if (cancel)
        {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        }

        else
        {
            User user = new User(username, password);
            getTokenUser(user);
        }
    }

    public void getTokenUser(final User user)
    {
        Call<Token> call = userService.getTokenUserPost(user);
        call.enqueue(new Callback<Token>()
        {
            @Override
            public void onResponse(Call<Token> call, Response<Token> response)
            {
                if(response.isSuccessful())
                {
                    userToken = response.body();
                    sharedPreferencesManager.setTokenApi("Token " + userToken.getToken());
                    getUserAuth(user);
                    Intent intentListPosts = new Intent(MainActivity.this, ListPostActivity.class);
                    startActivity(intentListPosts);
                }
            }

            @Override
            public void onFailure(Call<Token> call, Throwable t)
            {
                Log.e("ERROR: ", t.getMessage());
            }
        });
    }

    public void getUserAuth(User user)
    {
        Call<UserAuth> call = userAuthService.getUserAuth(user.getUsername(), user.getPasword());
        call.enqueue(new Callback<UserAuth>()
        {
            @Override
            public void onResponse(Call<UserAuth> call, Response<UserAuth> response)
            {
                if(response.isSuccessful())
                {
                    userAuth = response.body();
                    sharedPreferencesManager.setIdUser(String.valueOf(userAuth.getId()));
                }
            }

            @Override
            public void onFailure(Call<UserAuth> call, Throwable t)
            {
                Log.e("ERROR: ", t.getMessage());
            }
        });
    }
}
