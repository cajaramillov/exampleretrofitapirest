package com.newmaster.loginactivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.newmaster.loginactivity.model.Post;
import com.newmaster.loginactivity.remote.PostService;
import com.newmaster.loginactivity.remote.ApiUtils;
import com.newmaster.loginactivity.shared_preferences.SharedPreferencesManager;


public class ListPostActivity extends AppCompatActivity
{
    private FloatingActionButton buttonAddPost;
    private ListView listPost;
    private PostService postService;
    private List<Post> list = new ArrayList<Post>();
    private String token;
    private SharedPreferencesManager sharedPreferencesManager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_post);
        setTitle("List Post");
        sharedPreferencesManager = new SharedPreferencesManager(getApplicationContext());
        token = sharedPreferencesManager.getTokenApi();
        Log.e("Mirelo", token);
        buttonAddPost = (FloatingActionButton) findViewById(R.id.btn_add_post);
        listPost = (ListView) findViewById(R.id.post_list);
        postService = ApiUtils.getPostService();
        getPostsList();

        buttonAddPost.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(ListPostActivity.this, PostActivity.class);
                intent.putExtra("user_name", "");
                startActivity(intent);
            }
        });
    }

    public void getPostsList()
    {
        Call<List<Post>> call = postService.getPosts(token);
        call.enqueue(new Callback<List<Post>>()
        {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response)
            {
                if(response.isSuccessful())
                {
                    list = response.body();
                    listPost.setAdapter(new PostAdapter(ListPostActivity.this, R.layout.list_post, list));
                }
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t)
            {
                Log.e("ERROR: ", t.getMessage());
            }
        });
    }
}
