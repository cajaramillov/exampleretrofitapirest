package com.newmaster.loginactivity.remote;

public class ApiUtils
{
    private ApiUtils(){}

    public static final String API_URL = "http://192.168.0.3:8000/api/";

    public static PostService getPostService()
    {
        return RetrofitClient.getClient(API_URL).create(PostService.class);
    }

    public static UserService getUserService()
    {
        return RetrofitClient.getClient(API_URL).create(UserService.class);
    }

    public static UserAuthService getUserAuthService()
    {
        return RetrofitClient.getClient(API_URL).create(UserAuthService.class);
    }
}
