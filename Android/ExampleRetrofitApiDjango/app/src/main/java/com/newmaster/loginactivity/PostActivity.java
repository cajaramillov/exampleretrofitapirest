package com.newmaster.loginactivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.newmaster.loginactivity.model.Post;
import com.newmaster.loginactivity.remote.PostService;
import com.newmaster.loginactivity.remote.ApiUtils;
import com.newmaster.loginactivity.shared_preferences.SharedPreferencesManager;

public class PostActivity extends AppCompatActivity
{
    private PostService postService;
    private EditText edtTitle;
    private EditText edtBody;
    private EditText edtIdUser;
    private Button btnSave;
    private Button btnUpdate;
    private Button btnDelete;
    private String token;
    private SharedPreferencesManager sharedPreferencesManager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        setTitle("Post");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sharedPreferencesManager = new SharedPreferencesManager(getApplicationContext());
        token = sharedPreferencesManager.getTokenApi();
        edtTitle = (EditText) findViewById(R.id.text_title);
        edtBody = (EditText) findViewById(R.id.text_body);
        edtIdUser = (EditText) findViewById(R.id.text_id_user);
        edtIdUser.setEnabled(false);
        btnSave = (Button) findViewById(R.id.button_save_post);
        btnUpdate = (Button) findViewById(R.id.button_update_post);
        btnDelete = (Button) findViewById(R.id.button_delete_post);
        postService = ApiUtils.getPostService();
        Bundle extras = getIntent().getExtras();
        assert extras != null;
        final String postId = extras.getString("post_id");
        String postTitle = extras.getString("post_title");
        String postBody = extras.getString("post_body");
        Integer postIdUser = Integer.parseInt(sharedPreferencesManager.getIdUser());
        edtTitle.setText(postTitle);
        edtBody.setText(postBody);
        edtIdUser.setText(String.valueOf(postIdUser));

        if(postId == null || postId.trim().length() == 0 )
        {
            btnUpdate.setVisibility(View.INVISIBLE);
            btnDelete.setVisibility(View.INVISIBLE);
        }

        else
        {
            btnSave.setVisibility(View.INVISIBLE);
        }

        btnSave.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Post post = new Post();
                post.setTitle(edtTitle.getText().toString());
                post.setBody(edtBody.getText().toString());
                post.setIdUser(Integer.parseInt(edtIdUser.getText().toString()));
                //add user
                addUser(post);
                Intent intent = new Intent(PostActivity.this, ListPostActivity.class);
                startActivity(intent);
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Post post = new Post();
                post.setTitle(edtTitle.getText().toString());
                post.setBody(edtBody.getText().toString());
                post.setIdUser(Integer.parseInt(edtIdUser.getText().toString()));
                //update user
                assert postId != null;
                updateUser(Integer.parseInt(postId), post);
                Intent intent = new Intent(PostActivity.this, ListPostActivity.class);
                startActivity(intent);
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                assert postId != null;
                deleteUser(Integer.parseInt(postId));
                Intent intent = new Intent(PostActivity.this, ListPostActivity.class);
                startActivity(intent);
            }
        });
    }

    public void addUser(Post post)
    {
        Call<Post> call = postService.addPost(token, post);

        call.enqueue(new Callback<Post>()
        {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response)
            {
                if(response.isSuccessful())
                {
                    Toast.makeText(PostActivity.this, "Post created successfully!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t)
            {
                Log.e("ERROR: ", t.getMessage());
            }
        });
    }

    public void updateUser(int id, Post post)
    {
        Call<Post> call = postService.updatePost(token, id, post);

        call.enqueue(new Callback<Post>()
        {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response)
            {
                if(response.isSuccessful())
                {
                    Toast.makeText(PostActivity.this, "User updated successfully!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t)
            {
                Log.e("ERROR: ", t.getMessage());
            }
        });
    }

    public void deleteUser(int id)
    {
        Call<Post> call = postService.deletePost(token, id);

        call.enqueue(new Callback<Post>()
        {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response)
            {
                if(response.isSuccessful())
                {
                    Toast.makeText(PostActivity.this, "User deleted successfully!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t)
            {
                Log.e("ERROR: ", t.getMessage());
            }
        });
    }
}