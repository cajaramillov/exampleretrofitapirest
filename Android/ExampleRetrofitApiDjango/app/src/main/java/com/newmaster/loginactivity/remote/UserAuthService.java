package com.newmaster.loginactivity.remote;

import com.newmaster.loginactivity.model.UserAuth;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;


public interface UserAuthService
{
    @GET("user/{username}/{password}/")
    Call<UserAuth> getUserAuth(@Path("username") String username, @Path("password") String password);
}
