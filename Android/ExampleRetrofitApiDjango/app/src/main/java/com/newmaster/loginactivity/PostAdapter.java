package com.newmaster.loginactivity;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.newmaster.loginactivity.model.Post;


public class PostAdapter extends ArrayAdapter<Post>
{
    private Context context;
    private List<Post> posts;

    public PostAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<Post> objects)
    {
        super(context, resource, objects);
        this.context = context;
        this.posts = objects;
    }

    @Override
    public View getView(final int pos, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_post, parent, false);

        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt_title);
        TextView txtBody = (TextView) rowView.findViewById(R.id.txt_body);

        txtTitle.setText(String.format("Title: %s", posts.get(pos).getTitle()));
        txtBody.setText(String.format("Body: %s", posts.get(pos).getBody()));

        rowView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //start Activity User Form
                Intent intent = new Intent(context, PostActivity.class);
                intent.putExtra("post_id", String.valueOf(posts.get(pos).getId()));
                intent.putExtra("post_title", posts.get(pos).getTitle());
                intent.putExtra("post_body", posts.get(pos).getBody());
                context.startActivity(intent);
            }
        });

        return rowView;
    }


}
