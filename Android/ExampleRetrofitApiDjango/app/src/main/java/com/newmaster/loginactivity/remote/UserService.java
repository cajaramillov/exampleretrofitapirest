package com.newmaster.loginactivity.remote;

import com.newmaster.loginactivity.model.Token;
import com.newmaster.loginactivity.model.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;


public interface UserService
{
    @POST("login/")
    Call<Token> getTokenUserPost(@Body User user);
}
