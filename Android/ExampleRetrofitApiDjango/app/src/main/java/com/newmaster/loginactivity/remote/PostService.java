package com.newmaster.loginactivity.remote;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

import com.newmaster.loginactivity.model.Post;


public interface PostService
{
    @GET("post/")
    Call<List<Post>> getPosts(@Header("Authorization") String authorization);

    @POST("post/")
    Call<Post> addPost(@Header("Authorization") String authorization, @Body Post post);

    @PUT("post/{id}/")
    Call<Post> updatePost(@Header("Authorization") String authorization, @Path("id") int id, @Body Post post);

    @PATCH("post/{id}/")
    Call<Post> updatePartialPost(@Header("Authorization") String authorization, @Path("id") int id, @Body Post post);

    @DELETE("post/{id}/")
    Call<Post> deletePost(@Header("Authorization") String authorization, @Path("id") int id);
}
