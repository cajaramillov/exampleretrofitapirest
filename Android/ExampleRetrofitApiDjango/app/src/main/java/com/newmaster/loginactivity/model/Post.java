package com.newmaster.loginactivity.model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Post
{
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("body")
    @Expose
    private String body;
    @SerializedName("id_user")
    @Expose
    private Integer idUser;


    public Post() {}

    public Post(Integer id, String title, String body, Integer idUser)
    {
        super();
        this.id = id;
        this.title = title;
        this.body = body;
        this.idUser = idUser;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getBody()
    {
        return body;
    }

    public void setBody(String body)
    {
        this.body = body;
    }

    public Integer getIdUser()
    {
        return idUser;
    }

    public void setIdUser(Integer idUser)
    {
        this.idUser = idUser;
    }


}
