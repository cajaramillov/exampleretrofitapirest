package com.newmaster.loginactivity.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User
{
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("password")
    @Expose
    private String pasword;

    public User() {}

    public User(String username, String pasword)
    {
        this.username = username;
        this.pasword = pasword;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPasword()
    {
        return pasword;
    }

    public void setPasword(String pasword)
    {
        this.pasword = pasword;
    }
}
