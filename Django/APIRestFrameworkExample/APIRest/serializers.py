# -*- encoding: utf-8 -*-
from rest_framework import serializers
from django.contrib.auth.models import User

from APIRest.models import Post


class PostSerializer(serializers.ModelSerializer):

    class Meta:
        model = Post
        fields = ('id','title','body','id_user')


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'username')