from django.urls import path

from APIRest.views import LoginView
from APIRest.views import UserView
from APIRest.views import PostView


urlpatterns = [
    path('login/', LoginView.as_view(), name="api-login"),
    path('post/', PostView.as_view(), name="api-posts"),
    path('post/<int:pk>/', PostView.as_view(), name="api-posts-detail"),
    path('user/<str:username>/<str:password>/', UserView.as_view(), name="api-user-detail"),
]